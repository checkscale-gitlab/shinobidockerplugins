#!/bin/sh
set -e

# Copy existing custom configuration files
echo "Copy custom configuration files ..."
if [ -d /config ]; then
    cp -R -f "/config/"* /opt/plugin || echo "No custom config files found." 
fi

# Create default configurations files from samples if not existing
if [ ! -f /opt/plugin/conf.json ]; then
    echo "Create default config file /opt/plugin/conf.json ..."
    cp /opt/plugin/conf.sample.json /opt/plugin/conf.json
fi

# Update Plugin's configuration by environment variables
echo "Updating Plugin's configuration to match your environment ..."

# Set configuration for plugin ...
#   TODO:   Modify this section to set the configuration for the plugin!
#           Have a look at the configuration for the Motion plugin below!
#{
#  "plug":"Motion",
#  "host":"localhost",
#  "port":8080,
#  "key":"change_this_to_something_very_random____make_sure_to_match__/plugins/motion/conf.json",
#  "notice":"Looks like you have the Motion plugin running. Don't forget to enable <b>Send Frames</b> to start pushing frames to be read."
#}
sed -i -e 's/"host":"localhost"/"host":"'"${PLUGIN_HOST}"'"/g' \
       -e 's/"port":8080/"port":"'"${PLUGIN_PORT}"'"/g' \
       -e 's/"key":"change_this_to_something_very_random____make_sure_to_match__\/plugins\/motion\/conf.json"/"key":"'"${PLUGIN_KEY}"'"/g' \
       "/opt/plugin/conf.json"

# Change the uid/gid of the node user
if [ -n "${GID}" ]; then
    if [ -n "${UID}" ]; then
        echo " - Set the uid:gid of the node user to ${UID}:${GID}"
        groupmod -g ${GID} node && usermod -u ${UID} -g ${GID} node
    fi
fi

# Show plugin's configuration
echo "The plugin's runtime configuration:"
cat /opt/plugin/conf.json
echo ""

# Execute Command
echo "Starting plugin ..."
exec "$@"
